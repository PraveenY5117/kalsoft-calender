// Initializes the `Uploads` service on path `/uploads`
const { Uploads } = require('./uploads.class');
const createModel = require('../../models/uploads.model');
const hooks = require('./uploads.hooks');

//const filters = require('./uploads.filters');
const blobService = require('feathers-blob');
const fs = require('fs-blob-store');

const blobStorage = fs('./uploads');

module.exports = function (app) {
  const options = {
    Model: createModel(app),
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/uploads', new Uploads(options, app));
  app.use('/uploads', blobService({ Model: blobStorage}));

  // Get our initialized service so that we can register hooks
  const service = app.service('uploads');
  

  service.hooks(hooks);

  // if (service.filter) {
  //   service.filter(filters);
  // }
};
