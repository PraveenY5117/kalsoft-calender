const users = require('./users/users.service.js');
const demo = require('./demo/demo.service.js');
const uploads = require('./uploads/uploads.service.js');
const login = require('./login/login.service.js');
// eslint-disable-next-line no-unused-vars
module.exports = function (app) {
  app.configure(users);
  app.configure(demo);
  app.configure(uploads);
  app.configure(login);
};
